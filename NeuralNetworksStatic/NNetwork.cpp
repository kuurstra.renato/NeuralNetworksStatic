#include "stdafx.h"

#include "NNetwork.h"

NNetwork::NNetwork()
{

}

NNetwork::~NNetwork()
{
	if (!IsInitialized) return;

	size_t NNsize = NeuralNetwork.size();

	for (size_t i = 0; i < NNsize; i++)
	{
		size_t NNLayersize = NeuralNetwork[i].size();

		for (size_t j = 0; j < NNLayersize; j++)
		{
			delete NeuralNetwork[i][j];
		}
	}

}

void NNetwork::PrintNetworkCsv(NNetwork* A, std::string& FileName, bool close)
{
	//if (LogsKeeper::getInstance().WriteLog(FileName, "") == false) LogsKeeper::getInstance().CreateNew(FileName, "csv");

	int NetSize = A->NeuralNetwork.size();

	std::stringstream message;
	message << "New network layers: " << NetSize << std::endl;
	message << "Ideal Outputs: ";
	for (int i = 0; i < A->LastIdealOutputs.size(); i++)
	{
		message << A->LastIdealOutputs[i] << ", ";
	}

	for (int i = 0; i < NetSize; i++)
	{
		int LayerSize = A->NeuralNetwork[i].size();
		message << std::endl << "Layer: " << i << " size: " << LayerSize << std::endl;
		for (int j = 0; j < LayerSize; j++)
		{
			message << "	Neuron: " << A->NeuralNetwork[i][j]->get_posinlayer() << "      ";

			message << A->NeuralNetwork[i][j]->get_value() << "," << A->NeuralNetwork[i][j]->get_error() << "    weights: ";

			std::vector<float> weights = A->NeuralNetwork[i][j]->GetWeights();
			int Wsize = weights.size();
			for (int o = 0; o < Wsize; o++)
			{
				message << weights[o] << ", ";
			}
			message << std::endl;

		}
	}
	LogsKeeper::getInstance().Log(message.str());
}

void NNetwork::LerpNetworks(NNetwork* A, NNetwork* B, float alpha)
{
	size_t netsize = A->NeuralNetwork.size();

	for (size_t i = 0; i < netsize; i++)
	{
		size_t layesize = A->NeuralNetwork[i].size();

		for (size_t j = 0; j < layesize; j++)
		{
			A->NeuralNetwork[i][j]->LerpNeuron(A->NeuralNetwork[i][j], alpha);
		}
	}
}

void NNetwork::OneRandomNeuron(NNetwork* A, NNetwork* B, float alpha)
{
	size_t netsize = A->NeuralNetwork.size();

	for (size_t i = 0; i < netsize; i++)
	{
		size_t layesize = A->NeuralNetwork[i].size();

		for (size_t j = 0; j < layesize; j++)
		{
			if (MatAndRandom::getInstance().rea0_1() <= alpha)
				A->NeuralNetwork[i][j]->CopyNeuronWeights(B->NeuralNetwork[i][j]);
		}
	}

}

void NNetwork::CopyNetworks(NNetwork* A, NNetwork* B)
{
	size_t netsize = A->NeuralNetwork.size();

	for (size_t i = 0; i < netsize; i++)
	{
		size_t layesize = A->NeuralNetwork[i].size();

		for (size_t j = 0; j < layesize; j++)
		{
			A->NeuralNetwork[i][j]->CopyNeuronWeights(B->NeuralNetwork[i][j]);
		}
	}
}

void NNetwork::shape_init(const std::vector<int>& NetWorkshape, bool isbiased, bool islstm)
{

	size_t NNsize = NetWorkshape.size();
	IsBiased = isbiased;
	IsLSTM = islstm;

	NeuralNetwork.resize(NNsize);

	for (size_t i = 0; i < NNsize; i++)
	{
		//if is biased we make one more neuron, so the layer size will be greater
		//if it's the last layer, we don't add the bias
		size_t NNLayersize = NetWorkshape[i] + (IsBiased == true ? 1 : 0);

		NeuralNetwork[i].resize(NNLayersize);

		for (size_t j = 0; j < NNLayersize; j++)
		{

			//09/07/17			//NeuralNetwork[i][j] = new Neuron( static_cast<int> ( j ) );

			if (i == 0)
			{
				if (j == 0 && IsBiased == true) // it means is the bias
				{
					NeuralNetwork[i][j] = new Neuron_Bias(i, j);
					NeuralNetwork[i][j]->set_value(1.0f); // Necessary for now, this way input layer bias will have correct value
				}
				else
				{
					NeuralNetwork[i][j] = new Neuron_Input(i, j);
				}

				continue;
			}


			if (j == 0 && IsBiased == true) // it means is the bias
			{
				NeuralNetwork[i][j] = new Neuron_Bias(i, j);
			}
			else
			{
				NeuralNetwork[i][j] = (IsLSTM == true ? new Neuron_LSTM(i, j) : new Neuron(i, j));
			}

		}
	}

	IsInitialized = true;
}

// Setting up the linking of the network. First layer(input) is ignored
void NNetwork::synapse_init(bool HasDistrubRandom)
{
	if (IsInitialized == false)
	{
		printf("Error: Asking to initialize synapsis without first init the network");
		return;
	}

	size_t NNsize = NeuralNetwork.size();

	for (size_t i = 1; i < NNsize; i++)
	{
		size_t NNLayersize = NeuralNetwork[i].size();

		for (size_t j = 0; j < NNLayersize; j++)
		{
			if (IsBiased == true && j == 0)
			{
				continue;
			}
			//First we create the synapsis
			//std::vector< Neuron* > previous(std::begin(NeuralNetwork[i - 1]), std::end(NeuralNetwork[i - 1]));
			//std::vector< Neuron* > current(std::begin(NeuralNetwork[i]), std::end(NeuralNetwork[i]));
			std::vector< Neuron* > previous = NeuralNetwork[i - 1];
			std::vector< Neuron* > current = NeuralNetwork[i];
			std::vector< Neuron* > next;
			if (i < NNsize - 1) next = NeuralNetwork[i + 1];
			if (IsLSTM == true) previous.insert(previous.end(), current.begin(), current.end());

			NeuralNetwork[i][j]->synapsis(previous, next);
			NeuralNetwork[i][j]->randomize_weights(HasDistrubRandom);
		}

	}
}


bool NNetwork::SetInputLayer(std::vector<float>& inputs)
{

	if (inputs.size() != NeuralNetwork[0].size() - (IsBiased == true ? 1 : 0))
	{
		return false;
	}

	size_t NNInputSize = NeuralNetwork[0].size();

	//If biased we need start at the second neuron
	for (size_t i = (IsBiased == true ? 1 : 0); i < NNInputSize; i++)
	{
		NeuralNetwork[0][i]->set_value(inputs[i - (IsBiased == true ? 1 : 0)]);
	}

	return true;
}

std::vector<float> NNetwork::evaluate()
{
	size_t NNsize = NeuralNetwork.size();

	//We skip the input layer, so we start from index 1
	for (size_t i = 1; i < NNsize; i++)
	{
		size_t NNLayersize = NeuralNetwork[i].size();

		for (size_t j = 0; j < NNLayersize; j++)
		{
			NeuralNetwork[i][j]->evaluate_tanh();
		}
	}

	// Necessary for fully compatibility with RNN's using LSTM(they need last input)
	// probably it would be better to make something specifically suited for them,
	// instead of using the same soltion for both RNN and non RNN.
	for (size_t i = 0; i < NNsize; i++)
	{
		size_t NNLayersize = NeuralNetwork[i].size();

		for (size_t j = 0; j < NNLayersize; j++)
		{
			// Move the lastvalidvalue(Of previous evaluate) to PreviousValue
			// then moving value to lastvalidvalue.
			NeuralNetwork[i][j]->set_LastValidValue();
		}
	}

	std::vector<float> return_value;
	size_t NN_output_size = NeuralNetwork[NNsize - 1].size();

	return_value.resize(NN_output_size - (IsBiased == true ? 1 : 0));

	// Start from neuron 1 if it's a biased network
	for (size_t i = (IsBiased == true ? 1 : 0); i < NN_output_size; i++)
	{
		return_value[i - (IsBiased == true ? 1 : 0)] = NeuralNetwork[NNsize - 1][i]->get_value();
	}

	return return_value;
}


void NNetwork::NetworkBackPropagation(int type, const std::vector<float>& output_ideal, float learnrate)
{
	// From the last layer to the first layer!
	LastIdealOutputs = output_ideal;
	// auto is safe, because it's just a iterator for a vector of vector
	for (auto it = NeuralNetwork.rbegin(); it != NeuralNetwork.rend(); it++)
	{
		// auto is safe, because it's just a iterator for a vector of pointers
		for (auto it2 = it->begin(); it2 != it->end(); it2++)
		{
			if (IsBiased == true)
			{
				if (NeuralNetwork.rbegin() == it && it2 != it->begin() ) (*it2)->NeuronBackPropagation(type, output_ideal[it2 - it->begin() - 1], learnrate);
				else (*it2)->NeuronBackPropagation(type, 0.0, learnrate);
			}
			else
			{
				if (NeuralNetwork.rbegin() == it) (*it2)->NeuronBackPropagation(type, output_ideal[it2 - it->begin()], learnrate);
				else (*it2)->NeuronBackPropagation(type, 0.0, learnrate);
			}
		}
	}
}

void NNetwork::Mutate(float rate, int type, float str)
{
	for (auto it = NeuralNetwork.begin(); it != NeuralNetwork.end(); it++)
	{
		for (auto it2 = it->begin(); it2 != it->end(); it2++)
		{
			(*it2)->NeuronMutate(rate, type, str);
		}
	}
}

std::vector<int> NNetwork::GetShape()
{

	std::vector<int> ret_value;
	ret_value.resize(NeuralNetwork.size());
	for (int i = 0; i < NeuralNetwork.size(); i++)
	{
		ret_value[i] = (int)NeuralNetwork[i].size();
	}
	return ret_value;

}