#pragma once

#include <stdio.h>
#include <math.h>
#include <time.h>
#include <random>
#include <chrono>

class MatAndRandom
{
public:

	static float lerpf(float a, float b, float alpha);

	static MatAndRandom& getInstance()
	{
		static MatAndRandom    instance; // Guaranteed to be destroyed.
							  // Instantiated on first use.
		return instance;
	}
private:
	MatAndRandom()
	{
		Rng.seed(RandomDevice());
	}

	std::mt19937_64 Rng;
	std::random_device RandomDevice;

public:
	MatAndRandom(MatAndRandom const&) = delete;
	void operator=(MatAndRandom const&) = delete;

	void ReSeed( unsigned Seed);

	int intn_n(int min, int max);
	double rean_n(double min, double max);
	double rea0_1();
	double rea1_1();
	int sign_1_1();

};