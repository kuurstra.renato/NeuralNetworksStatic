#pragma once

#include <unordered_map>
#include <iostream>
#include <fstream>

class LogsKeeper
{
public:
	LogsKeeper();
	~LogsKeeper();

	static LogsKeeper& getInstance()
	{
		static LogsKeeper    instance; // Guaranteed to be destroyed.
									   // Instantiated on first use.
		return instance;
	}

	LogsKeeper(LogsKeeper const&) = delete;
	void operator=(LogsKeeper const&) = delete;
	void CreateNew(std::string filename, std::string extension);
	bool WriteLog(std::string file, std::string message);
	bool Log( std::string message );

private:
	//Static variable initiation
	std::unordered_map< std::string, std::ofstream> Logs;
};

