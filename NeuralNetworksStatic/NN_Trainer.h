// This File is meant to collect declarations and prototypes for a
// Neural Network Trainer; following R. Hecht-Nielsen,
// "Neurocomputing", Addison-Wesley, 1990, there are three categories the trainer may belong to
//
// 1. Supervised training, where input and output for training set are
// user defined, and network is trained with one of the many variants
// of Hebbian Back-Propagation formula;
//
// 2. Graded (or reinforcement) training
//
// 3. Self-organization

#pragma once

#include <vector>

typedef structure training_pattern
{
  vector<float> i;
  vector<float> o;
} training_pattern_r;

typedef vector<training_pattern> training_pattern_vector;


virtual class trainer (NNetwork ntbt,
                       training_pattern_vector training_set,
                       training_pattern_vector validation_set,
                       float learning_rate,
                       float momentum,
                       float convergence_tolerance) {
  training_pattern_vector _test_set;
  training_pattern_vector _training_set;
  training_pattern_vector _validation_set;
  
  NNetwork _ntbt; // network to be trained...
  
  _ntbt           = ntbt;
  _training_set   = training_set;
  _validation_set = validation_set;
  _learning_rate  = learning_rate;
  _momentum       = momentum;
  
  ///
  // training has to be repeated until either convergence is reached,
  // or a given limit in iteration is hit without convergence
  // 
  float train_individually() {
    foreach t (_training_set) {
      _ntbt.forward_propagate(t.i);
      _ntbt.compute_error(t.o);
      _ntbt.back_propagate(learning_rate,momentum);
    }
  }
  float train_batch() {
    foreach t (_training_set) {
      _ntbt.forward_propagate(t.i);
      _ntbt.compute_error(t.o);
      accumulated_error=_ntbt.tell_error();
    }
    mean_accumulated_error=mediate_on_size_of_training_set(_training_set.size,accumulated_error)
      _ntbt.back_propagate();
  }

  // Forward propagate and compute global error for a set of training patterns

}
