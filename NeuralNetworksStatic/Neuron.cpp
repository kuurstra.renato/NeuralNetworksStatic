#include "stdafx.h"
#include "Neuron.h"

/*
Neuron::Neuron( int Enum_SquashType)
{
	SquashType = static_cast<SquashFormula>(Enum_SquashType);
}
*/

Neuron::Neuron( int layer,  int posinlayer)
{
	PosInLayer = posinlayer;
	Layer = layer;
}

Neuron::~Neuron()
{

}

float Neuron::get_value()
{
	return LastValidValue;
}

void Neuron::set_value(float val) // usually used to set the input layer
{
	LastValidValue = PreviousValue = Value = val;
}

void Neuron::set_LastValidValue()
{
	PreviousValue = LastValidValue;
	LastValidValue = Value;
}

float Neuron::get_error()
{
	return LastValidError;
}

void Neuron::set_error(float val) 
{
	LastValidError = Error = PreviosError = val;
}

void Neuron::set_LastValidError()
{
	PreviosError = LastValidError;
	LastValidError = Error;
}

int Neuron::get_posinlayer()
{
	return PosInLayer;
}

std::vector<float> Neuron::GetWeights()
{
	return _weights;
}

void Neuron::randomize_weights(bool distrib_control)
{
	size_t ListLength = _connections.size();

	if (ListLength == 0)
	{
		printf("Neuron, size of weights is 0");
		return;
	}

	float deltarandom = 0.0f;
	if (distrib_control == true)
	{
		deltarandom = 1.0f / (float)ListLength;
	}

	for (size_t i = 0; i < ListLength; i++)
	{
		if (distrib_control == true)
		{
			_weights[i] = MatAndRandom::getInstance().rean_n(-deltarandom, deltarandom);
		}
		else
		{
			_weights[i] = MatAndRandom::getInstance().rea1_1();
		}
	}

}

void Neuron::synapsis(std::vector< Neuron* >& previouslayer, std::vector< Neuron* >& nextlayer)
{
	_connections = previouslayer;
	
	_weights.resize(previouslayer.size());

	_backConnections = nextlayer;
}

float Neuron::evaluate_tanh()
{
	float final_value = 0.0;
	size_t ListLength = _connections.size();

	for (size_t i = 0; i < ListLength; i++)
	{
		final_value += _connections[i]->Value * _weights[i];
	}

	final_value = tanh(final_value);

	Value = final_value;

	return final_value;
}

float Neuron::evaluate_tan_custom( float tanh_factor )
{
	float final_value = 0.0f;
	size_t ListLength = _connections.size();

	for (size_t i = 0; i < ListLength; i++)
	{
		final_value += _connections[i]->Value * _weights[i];
	}

	// Or we can try a fast aproximation function
	// x / ( 1.759 + abs(x) )  
	final_value = 2 / 1 + pow(tanh_factor, (final_value * -2));

	Value = final_value;

	return final_value;
}


float Neuron::evaluate_tanh_norm()
{
	float final_value = 0.0;
	size_t ListLength = _connections.size();

	for (size_t i = 0; i < ListLength; i++)
	{
		final_value += _connections[i]->Value * _weights[i];
	}

	final_value = tanh(final_value);

	final_value = ( final_value + 1.0 ) / 2.0;

	if (final_value < 0.0) final_value = 0.0;
	if (final_value > 1.0) final_value = 1.0;

	Value = final_value;

	return final_value;
}

void Neuron::LerpNeuron(Neuron* B, float alpha)
{
	size_t weightsize = _weights.size();
	for (size_t i = 0; i < weightsize; i++)
	{
		_weights[i] = MatAndRandom::lerpf(_weights[i], B->_weights[i], alpha);
	}
}

void Neuron::CopyNeuronWeights(Neuron* B)
{
	_weights = B->_weights;
}

void Neuron::NeuronBackPropagation(int type, float output_ideal, float learnrate)
{
	if ((EBPType)type == EBPType::SimpleBP)
	{
		size_t nweights = _weights.size();

		if (_backConnections.size() == 0) // No dimension of BackConnections means it's the output layer!
		{
			// partial derivate of total error
			double pderTerror = get_value() - output_ideal;
			// partial derivate of logistic function
			
			//this one is used in logistic functions
			//double pderLfunc = get_value() * (1.0 - get_value());

			double pderLfunc = 1.0 - pow( tanh(get_value()), 2.0 );

			//useless optimization?
			double pderTL = pderTerror * pderLfunc * learnrate;


			for (size_t i = 0; i < nweights; i++)
			{
				// pderTerror * pderLfunc * value of neuron(output of neuron) == deltarule
				_weights[i] -= pderTL * _connections[i]->get_value();
			}
			set_error( pderLfunc * pderTerror );
		}
		else
		{
			// For hidden layer
			// need pderTerror * pderLfunc * weight first NEXT layer neuron + same for EACH next layer neuron
			double derTerror = 0.0;

			for (auto it = _backConnections.begin(); it != _backConnections.end(); it++)
			{
				if ((*it)->_weights.size() == 0) continue;

				derTerror += (*it)->_weights[PosInLayer] * (*it)->LastValidError;
			}

			//double pderLfunc = get_value() * (1.0 - get_value());

			double pderLfunc = 1.0 - pow(tanh(get_value()), 2.0);

			double pderTL = derTerror * pderLfunc * learnrate;

			for (size_t i = 0; i < nweights; i++)
			{
				_weights[i] -= pderTL * _connections[i]->get_value();
			}

			set_error( derTerror * pderLfunc );
			// then FOR EACH weight calculate pderLfunc of current weights and get_value of neuron
			// multiply all togheder
		}
	}
}

void Neuron::NeuronMutate(float rate, int type, float str)
{
	for (auto it = _weights.begin(); it != _weights.end(); it++)
	{
		if ( MatAndRandom::getInstance().rea0_1() <= rate )
		{
			switch (type)
			{
			case 0:
				*it = MatAndRandom::getInstance().rea1_1();
				break;
			case 1:
				*it += *it * MutationFactor * str * MatAndRandom::getInstance().sign_1_1();
				break;
			default:
				*it = MatAndRandom::getInstance().rea1_1();
				break;
			}
			
		}
	}
}

/*    BIAS NEURON    */


/*     LSTM NEURON     */
//
//Neuron_LSTM::Neuron_LSTM(int type) : Neuron(type)
//{
//
//}

void Neuron_LSTM::synapsis( std::vector< Neuron* >& PrevPlusCurrent, std::vector< Neuron* >& nextlayer)
{
	_connections = PrevPlusCurrent;

	_weights.resize( PrevPlusCurrent.size() );

	InputGate.synapsis( PrevPlusCurrent, nextlayer );
	ForgetGate.synapsis( PrevPlusCurrent, nextlayer );
	OutputGate.synapsis( PrevPlusCurrent, nextlayer );
}

void Neuron_LSTM::randomize_weights(bool distrib_control)
{
	Neuron::randomize_weights(distrib_control);

	InputGate.randomize_weights(distrib_control);
	ForgetGate.randomize_weights(distrib_control);
	OutputGate.randomize_weights(distrib_control);
}

float Neuron_LSTM::evaluate_tanh()
{
/* Valid for all neurons: 
   Collect current state neural network values(previous layer) and previous state neural network(current layer) and squash tanh
*/


// Evaluate Forget gate as Inputgate. <--- Should only set his value using the previous value of the current layer, but i try this way.
// First operation is checking the forget gate, based on last iteration
	ForgetGate.evaluate_tanh_norm();
	ForgetGate.set_LastValidValue();


	// Multiply NeuronState by ForgetGateEvaluation and Set the ErrorCarousel
	ErrorCarousel = ErrorCarousel * ForgetGate.get_value();

// Evaluate neuron status(as per general rule)  NeuronState

	Neuron::evaluate_tanh(); // this will generate the current NeuronState inside Value

// Evaluate InputGate (Squash with normalized Tanh 0-1), also consider current NeuronState
	InputGate.evaluate_tanh_norm(); // we calculate the Value of this neuron as a 0-1 (gate) value
	InputGate.set_LastValidValue(); // we directly set the last validvalue to the Value obtained(no need previous data for gates)

// Multiply NeuronState by InputGateEvaluation and we add it to the error carousel
// Set Error carousel

	ErrorCarousel += Value * InputGate.get_value();


// Evaluate OutputGate as Inputgate.
	OutputGate.evaluate_tanh_norm();
	OutputGate.set_LastValidValue();

// Multiply NEuronState by OutputGateEvaluation ----> Neuron Output!
	//forgot to tanh the error carosel BEFORE multiply by output gate(normalized values)
	Value = tanh( ErrorCarousel ) * OutputGate.get_value();

	return Value;
}

void Neuron_LSTM::NeuronMutate(float rate, int type, float str)
{
	Neuron::NeuronMutate(rate, type, str);

	ForgetGate.NeuronMutate(rate, type, str);
	InputGate.NeuronMutate(rate, type, str);
	OutputGate.NeuronMutate(rate, type, str);
}

void Neuron_LSTM::LerpNeuron(Neuron* B, float alpha)
{
	Neuron::LerpNeuron(B, alpha);

	Neuron_LSTM* Bplus = static_cast<Neuron_LSTM*>(B);

	InputGate.LerpNeuron(&Bplus->InputGate, alpha);
	ForgetGate.LerpNeuron(&Bplus->ForgetGate, alpha);
	OutputGate.LerpNeuron(&Bplus->OutputGate, alpha);
}


void Neuron_LSTM::CopyNeuronWeights(Neuron* B)
{
	Neuron::CopyNeuronWeights( B );

	Neuron_LSTM* Bplus = static_cast<Neuron_LSTM*>(B);

	InputGate.CopyNeuronWeights( &Bplus->InputGate );
	ForgetGate.CopyNeuronWeights( &Bplus->ForgetGate );
	OutputGate.CopyNeuronWeights( &Bplus->OutputGate );
}
