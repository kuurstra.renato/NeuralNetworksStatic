#include "stdafx.h"
#include "LogsKeeper.h"


LogsKeeper::LogsKeeper()
{
	Logs.emplace(std::pair< std::string, std::ofstream>( std::string("Base"), std::ofstream("BaseLog.txt", std::ofstream::out | std::ofstream::trunc) ));
}

LogsKeeper::~LogsKeeper()
{
	for (auto it = Logs.begin(); it != Logs.end(); it++)
	{
		it->second.close();
	}
}

void LogsKeeper::CreateNew(std::string filename = "NewFile", std::string extension = "" )
{
	Logs.emplace(std::pair< std::string, std::ofstream>(filename, std::ofstream( (extension == "" ) ? filename : filename + "." + extension, std::ofstream::out | std::ofstream::app)));
}

bool LogsKeeper::WriteLog(std::string file, std::string message)
{
	try {
		Logs.at(file) << message.c_str() << "\n";
	}
	catch (const std::out_of_range& oor)
	{
		return false;
	}
	return true;
}

bool LogsKeeper::Log(std::string message)
{
	Logs.at("Base") << message.c_str() << "\n";
	return true;
}
