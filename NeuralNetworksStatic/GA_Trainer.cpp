#include "stdafx.h"
#include "GA_Trainer.h"


GA_Trainer::GA_Trainer()
{
}


GA_Trainer::~GA_Trainer()
{
	for each (Solution* elem in Solutions)
	{
		delete elem;
	}
}

GA_Trainer::Solution::Solution()
{
	network = new NNetwork();
}

GA_Trainer::Solution::~Solution()
{
	delete network;
}		

GA_Trainer::EliteTable::EliteTable() 
{
}

GA_Trainer::EliteTable::~EliteTable() 
{
}

int GA_Trainer::EliteTable::RouletteWheel(float roulettevalue)
{
	float totalsofar = 0.0f;

	for (auto it = EliteSolutions.begin(); it != EliteSolutions.end(); it++)
	{
		if (totalsofar > roulettevalue)
			return it->second;
		else
			totalsofar += it->first;
	}

	return -1;
}

// Check if the solution proposed is more fit than any other inside the table
// Has to set IsElite when the return value is true
std::pair<int, bool> GA_Trainer::EliteTable::CheckToInsert(int solution, float fitness)
{
	std::pair<int, bool> ret_value(-1, false);

	if (fitness < MinRawFitness)
		return ret_value;

	
	if ( EliteSolutions.emplace(std::pair<float, int>(fitness, solution)).second == false) return ret_value;

	ret_value.second = true;

	if (EliteSolutions.size() > ElitesMax)
	{
		ret_value.first = EliteSolutions.begin()->second;
		EliteSolutions.erase( EliteSolutions.begin() );
		MinRawFitness = EliteSolutions.begin()->first;
	}

	TotalEliteFitness = 0.0f;

	for (auto it = EliteSolutions.begin(); it != EliteSolutions.end(); it++)
	{
		TotalEliteFitness += it->first;
	}
#ifdef DEBUG_PRINT
	std::stringstream test;
	test << "Total Elite Solutions: " << EliteSolutions.size()
		<< " Total Elite fitness: " << TotalEliteFitness
		<< " Least fitness: " << MinRawFitness
		<< " Added solution: " << solution;

	LogsKeeper::getInstance().Log(test.str());
#endif
	return ret_value;
}


std::vector<int> GA_Trainer::EliteTable::Clear()
{
	std::vector<int> ret_value;

	for (auto it = EliteSolutions.begin(); it != EliteSolutions.end(); it++)
	{
		ret_value.push_back( it->second);
	}

	EliteSolutions.clear();
	return ret_value;
}

void GA_Trainer::Init(std::vector<int>& shape, int maxpopulation, int initialpopulation, bool islstm, bool israndomdist, bool isbias)
{
	MaxSolutions = maxpopulation;

	NetsShape = shape;
	IsLSTM = islstm;
	IsDistrib = israndomdist;
	IsBias = isbias;

	PopulateSolutions(initialpopulation, true);
}

void GA_Trainer::SetGeneticVariables(float baserawfitness, float breedrate, float mutationrate)
{
	BaseRawFitness = baserawfitness;
	BreedRate = breedrate;
	MutationRate = mutationrate;
}

void GA_Trainer::PopulateSolutions(int NIndividuals, bool IsResetting)
{

	if (IsResetting == true)
	{
		for each (Solution* elem in Solutions)
		{
			delete elem;
		}
		Solutions.clear();
		SolutionsIndexs.clear();
		MaxSolutionIndex = 0;
		TotalFitness = 0.0f;
	}

	for (size_t i = 0; i < NIndividuals; i++)
	{
		CreateSolution();
	}

}

int GA_Trainer::CreateSolution()
{
	Solution* cozysolution;

	cozysolution = new Solution();
	Solutions.push_back(cozysolution);

	// taking a iterator referencing his position in the list, necessary reference to deal with outside use
	cozysolution->position = std::next(Solutions.end(), -1);
	cozysolution->SolutionIndex = MaxSolutionIndex;
	cozysolution->network->shape_init(NetsShape, IsBias, IsLSTM);
	cozysolution->network->synapse_init(IsDistrib);
	cozysolution->raw_fitness = BaseRawFitness;
	TotalFitness += BaseRawFitness;

	SolutionsIndexs.emplace(cozysolution->SolutionIndex, cozysolution->position);

	MaxSolutionIndex++;

	return cozysolution->SolutionIndex;
}

int GA_Trainer::CopyIndividual(int solutionindex)
{
	int retvalue;

	retvalue = CreateSolution();

	std::list<Solution*>::iterator solution;
	try {
		solution = SolutionsIndexs.at(solutionindex);
	}
	catch (const std::out_of_range& oor)
	{
		solution = SolutionsIndexs.at(retvalue);

#ifdef DEBUG_PRINT
		std::stringstream message;
		message << "Error finding solution: " << solutionindex << " forcing random solution";
		LogsKeeper::getInstance().Log(message.str());
#endif
	}

	std::list<Solution*>::iterator newsolution = SolutionsIndexs.at(retvalue);

	NNetwork::CopyNetworks((*newsolution)->network, (*solution)->network);

	return retvalue;
}

std::vector<float> GA_Trainer::Eval_Solution(int solutionindex, std::vector<float>& inputlayer)
{
	std::list<Solution*>::iterator solution = SolutionsIndexs.at(solutionindex);

	/*	try {
			solution = SolutionsIndexs.at(solutionindex);
		}
		catch (const std::out_of_range& oor) {
			return std::vector<float>(2,0);
		}
		*/
	(*solution)->network->SetInputLayer(inputlayer);

	return (*solution)->network->evaluate();

}


std::vector<float> GA_Trainer::Eval_Solution_BackProp(int solutionindex, std::vector<float>& inputlayer, int type, std::vector<float>& output_ideal, float learnrate)
{
	std::list<Solution*>::iterator solution = SolutionsIndexs.at(solutionindex);
	std::vector<float> outputs = Eval_Solution(solutionindex, inputlayer);
	(*solution)->network->NetworkBackPropagation(type, output_ideal, learnrate);

	return outputs;
}

void GA_Trainer::PrintNet(int solutionindex, std::string& filename)
{
	std::list<Solution*>::iterator solution = SolutionsIndexs.at(solutionindex);
	NNetwork::PrintNetworkCsv((*solution)->network, filename );
}

void GA_Trainer::SetRawFitness(int solutionindex, float rawfitness)
{
	std::list<Solution*>::iterator solution = SolutionsIndexs.at(solutionindex);
	(*solution)->raw_fitness = rawfitness;
	RecalcTotalFitness();
}

void GA_Trainer::AddRawFitness(int solutionindex, float rawfitness)
{
	std::list<Solution*>::iterator solution = SolutionsIndexs.at(solutionindex);
	(*solution)->raw_fitness += rawfitness;
	RecalcTotalFitness();
}

std::pair<int,float> GA_Trainer::RouletteSelection()
{
	std::pair<int, float> ret_value;
	ret_value.first = -1;

	float roulettetotal = (float) MatAndRandom::getInstance().rean_n(0.0, TotalFitness);
	ret_value.second = roulettetotal;
	float totalsofar = 0.0f;
	ret_value.first = GetSolutionAtFitness(roulettetotal);

	return ret_value;
}

int GA_Trainer::GetSolutionAtFitness( float solutionatposition )
{
	int ret_value;
	ret_value = -1;

	float totalsofar = 0.0f;

	/*
	First check if the solution is a elite one
	*/
	ret_value = Elites.RouletteWheel(solutionatposition);

	if (ret_value != -1) return ret_value;

	totalsofar += Elites.TotalEliteFitness;

	for (auto it = Solutions.begin(); it != Solutions.end(); it++)
	{

		if (SolutionInUse.find((*it)->SolutionIndex) == SolutionInUse.end()) continue;

		totalsofar += (*it)->raw_fitness;


		//Check if it's within acceptable parameters. This is not really a super clean way to do it,
		//but it will make everything more robust.
		if (totalsofar > solutionatposition)
		{
			ret_value = (*it)->SolutionIndex;
		}
	}

	if (ret_value == -1)
	{
		ret_value = (*std::next(Solutions.end(), -1))->SolutionIndex; // if we are here without a index, it most likely mean we need
																			// to select the last value

#ifdef DEBUG_PRINT
		std::stringstream message;
		message << "Roulette fail, roulettetotal: " << solutionatposition << " totalsofar: " << totalsofar;
		LogsKeeper::getInstance().Log(message.str());
#endif
	}

	return ret_value;
}

int GA_Trainer::GetNewSolution()
{
	//Create a new solution still to 
	std::pair<int, float> rouletteres = RouletteSelection();
	int basesolution = rouletteres.first;
	int newsolution = CopyIndividual(basesolution);

	int partner = Breed(newsolution, rouletteres.second);

	std::list<Solution*>::iterator solptr = SolutionsIndexs.at(newsolution);

	float str = 0.0f;
	float cozyfitness = ( (*SolutionsIndexs[basesolution])->raw_fitness + (*SolutionsIndexs[partner])->raw_fitness ) / 2.0;
	int mutationoperator = MutationOperator;

	if (cozyfitness < ( MaxFitness / 5.0f ) || MaxFitness == 0.0f )
	{
		// Totally randomize the network weights
		mutationoperator = 0;
	}
	else
	{
		if (MaxFitness < cozyfitness)
		{
			mutationoperator = 0;
		}
		else
		{
			str = MaxFitness / cozyfitness;
		}
	}

#ifdef DEBUG_PRINT
	std::stringstream message;
	message << "Solution " << basesolution << " fitness: " << (*SolutionsIndexs[basesolution])->raw_fitness << " Solution " 
		<< partner << " fitness: " << (*SolutionsIndexs[partner])->raw_fitness << " Max fitness: " << MaxFitness << " Operator: "
		<< mutationoperator << " Cozyfitness: " << cozyfitness << " Str: " << str;
#endif

	Mutate(newsolution, mutationoperator, str);

#ifdef DEBUG_PRINT
	LogsKeeper::getInstance().Log(message.str());
#endif

	SolutionInUse.emplace(newsolution);
	return newsolution;
}

int GA_Trainer::Breed(int solution, float rouletteposition)
{
	std::list<Solution*>::iterator solptr = SolutionsIndexs.at(solution);

	if (MatAndRandom::getInstance().rea0_1() <= BreedRate)
	{
		// Partner is rouletteposition +  1/2 total fitness
		//rouletteposition = fmod( rouletteposition + (TotalFitness / 2), TotalFitness);
		int partner = GetSolutionAtFitness( fmod(rouletteposition + (TotalFitness / 2), TotalFitness) );

#ifdef DEBUG_PRINT
		std::stringstream message;
		message << "Chosen: " << rouletteposition << " Total fitness: " << TotalFitness << " Other half: " << fmod(rouletteposition + (TotalFitness / 2), TotalFitness);
		LogsKeeper::getInstance().Log(message.str());
#endif

		std::list<Solution*>::iterator partnerptr = SolutionsIndexs.at(solution);

		switch (BreedOperator)
		{
		case 0:
			LerpSolutions((*solptr), (*partnerptr));
			break;
		case 1:
			OneRandomNeuron((*solptr), (*partnerptr));
			break;

		default:
			break;
		}
		return partner;
	}

	return -1;
}

void GA_Trainer::Mutate(int solution, int mutationoperator, float str)
{
	switch (mutationoperator)
	{
	case 1:
	case 2:
		(*SolutionsIndexs.at(solution))->network->Mutate(MutationRate, mutationoperator, str);
		break;
	case 3:
		(*SolutionsIndexs.at(solution))->network->Mutate(MutationRate, 0, str);
		(*SolutionsIndexs.at(solution))->network->Mutate(MutationRate, 1, str);
		break;
	default:
		(*SolutionsIndexs.at(solution))->network->Mutate(MutationRate, 0, str);
		break;
	}
}

void GA_Trainer::KillSolution(int solution, float fitness)
{
	SetRawFitness(solution, fitness);
	if (SolutionInUse.find(solution) != SolutionInUse.end()) SolutionInUse.erase(solution);

	std::pair< int, bool> elitereturn = Elites.CheckToInsert(solution, fitness);
	if (elitereturn.second == true)
	{
		(*SolutionsIndexs.at(solution))->IsElite = true;
		if (elitereturn.first != -1)
		{
			try {
				(*SolutionsIndexs.at(elitereturn.first))->IsElite = false;
			}
			catch (const std::out_of_range& oor)
			{

#ifdef DEBUG_PRINT
				std::stringstream message;
				message << "Error, Removed Elite solution not found: " << elitereturn.first;
				LogsKeeper::getInstance().Log(message.str());
#endif
			}
		}
	}

	RecalcTotalFitness();
}

void GA_Trainer::ClearElite()
{
	RemoveFromElite( Elites.Clear() );
}

auto GA_Trainer::destroy_solution(std::list<Solution*>::iterator it)
{

	int solution = (*it)->SolutionIndex;

	if ((*it)->IsElite == true)
	{

#ifdef DEBUG_PRINT
		LogsKeeper::getInstance().Log("Attempt to destroy Elite solution, skipping");
#endif
		return it++;
	}

	delete (*it);

	it = Solutions.erase(it);
	SolutionsIndexs.erase(solution);


#ifdef DEBUG_PRINT
	std::stringstream message;
	message << "Solution Destroyed: " << solution;
	LogsKeeper::getInstance().Log(message.str());
#endif

	if (SolutionInUse.find(solution) != SolutionInUse.end()) SolutionInUse.erase(solution);

	RecalcTotalFitness();

	return it;
}

int GA_Trainer::TrimSolutions()
{
	int ret_value = 0;

	auto it = Solutions.begin();

	while (it != Solutions.end())
	{
		if (SolutionInUse.find((*it)->SolutionIndex) == SolutionInUse.end() && (*it)->IsElite == false)
		{
			it = destroy_solution(it);
			ret_value++;
		}
		else
		{
			it++;
		}
	}

	return ret_value;
}

float GA_Trainer::GetTotalFitness()
{
	RecalcTotalFitness();

	return TotalFitness;
}

void GA_Trainer::SetBreedOperator(int breedoperator)
{
	BreedOperator = breedoperator;
}

void GA_Trainer::SetMutationOperator(int mutationoperator)
{
	BreedOperator = mutationoperator;
}

std::pair< int, NNetwork*> GA_Trainer::GetNetwork(int solution)
{
	return std::pair< int, NNetwork* >(solution, (*SolutionsIndexs.at(solution))->network);
}

void GA_Trainer::LerpSolutions(Solution* A, Solution* B)
{
	NNetwork::LerpNetworks(A->network, B->network, 0.5f);
}

void GA_Trainer::OneRandomNeuron(Solution* A, Solution* B)
{
	NNetwork::OneRandomNeuron(A->network, B->network, 0.5f);
}

void GA_Trainer::RecalcTotalFitness()
{
	TotalFitness = 0.0f;
	MaxFitness = 0.0f;

	for (auto it = Solutions.begin(); it != Solutions.end(); it++)
	{

		if (SolutionInUse.find((*it)->SolutionIndex) != SolutionInUse.end() || (*it)->IsElite == true)
		{
			TotalFitness += (*it)->raw_fitness;

			if ((*it)->raw_fitness > MaxFitness )
				MaxFitness = (*it)->raw_fitness;
		}

	}

	AvrgFitness = TotalFitness / SolutionInUse.size();
}


void GA_Trainer::RemoveFromElite(std::vector<int>& solutions)
{
	for (auto it = solutions.begin(); it != solutions.end(); it++)
	{
		(*SolutionsIndexs[*it])->IsElite = false;
	}
}

/*
void GA_Trainer::destroy_solution(int solution)
{
	std::list<Solution*>::iterator it = SolutionsIndexs.at(solution);

	//	delete (*it);

	Solutions.erase(it);
	SolutionsIndexs.erase(solution);
	if (SolutionInUse.find(solution) != SolutionInUse.end()) SolutionInUse.erase(solution);

	RecalcTotalFitness();
}
*/
