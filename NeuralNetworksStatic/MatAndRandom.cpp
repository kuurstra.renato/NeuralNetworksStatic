#include "stdafx.h"
#include "MatAndRandom.h"


float MatAndRandom::lerpf(float a, float b, float alpha)
{
	return a * ( 1.0 - alpha ) + b * alpha;
}

void MatAndRandom::ReSeed( unsigned Seed )
{
	Rng.seed( Seed );
}

int MatAndRandom::intn_n(int min, int max)
{
	std::uniform_int_distribution<int> Dist(min, max);
	return Dist(Rng);
}

double MatAndRandom::rean_n(double min, double max)
{
	std::uniform_real_distribution<double> Dist(min, max);
	return Dist(Rng);
}

double MatAndRandom::rea0_1()
{
	std::uniform_real_distribution<double> Dist( 0.0, 1.0 );
	return Dist(Rng);
}

double MatAndRandom::rea1_1()
{
	std::uniform_real_distribution<double> Dist( -1.0, 1.0 );
	return Dist(Rng);
}


int MatAndRandom::sign_1_1()
{
	std::bernoulli_distribution Dist(0.5);
	if (Dist(Rng) == true)
	{
		return -1;
	}
	else
	{
		return +1;
	}
}